package com.xfdmao.fcat.coin.service;

import com.xfdmao.fcat.coin.entity.Strategy;
import com.qwrt.base.common.service.BaseService;

/**
 * Created by fier on 2018/09/20
 */
public interface StrategyService extends BaseService<Strategy>{
}
