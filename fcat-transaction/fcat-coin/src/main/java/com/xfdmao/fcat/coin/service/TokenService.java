package com.xfdmao.fcat.coin.service;

import com.xfdmao.fcat.coin.entity.Token;
import com.qwrt.base.common.service.BaseService;

/**
 * Created by fier on 2018/09/20
 */
public interface TokenService extends BaseService<Token>{
}
