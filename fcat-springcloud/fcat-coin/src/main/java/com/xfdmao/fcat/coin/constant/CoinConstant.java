package com.xfdmao.fcat.coin.constant;


/**
 * Created by fier on 2018/09/20
 */
public class CoinConstant {
    /**
     * 交易的费率
     */
    public static double takerFee = 0.0003;

    /**
     * 做多
     */
    public static String BUY = "buy";

    /**
     * 做空
     */
    public static String SELL = "sell";

    /**
     * 开仓
     */
    public static String OPEN = "open";

    /**
     * 平仓
     */
    public static String CLOSE = "close";

}
