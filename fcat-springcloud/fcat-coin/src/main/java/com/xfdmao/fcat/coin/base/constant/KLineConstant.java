package com.xfdmao.fcat.coin.base.constant;

/**
 * Created by cissa on 2019/7/27.
 */
public class KLineConstant {
    public static final String OPTTYPE_BIGGAIN = "平空-涨幅超过定值";
    public static final String OPTTYPE_PROTECTION = "平空-跌破保护因子";
    public static final String OPTTYPE_OVERMA = "平空-站上短期均线";
    public static final String OPTTYPE_OVERDOWNLEADGAIN = "平空-下影线过长";
    /**
     * 买入开仓
     */
    public static String BUYSELLSTATUS_BUY_OPEN = "BUY_OPEN";
    /**
     * 卖出平仓
     */
    public static String BUYSELLSTATUS_SELL_CLOSE = "SELL_CLOSE";

    /**
     * 卖出开仓
     */
    public static String BUYSELLSTATUS_SELL_OPEN = "SELL_OPEN";
    /**
     * 买入平仓
     */
    public static String BUYSELLSTATUS_BUY_CLOSE = "BUY_CLOSE";
}
